<?php
namespace App\Bitm\SEIP\Students;
use pdo;
class Students{
    public $id='';
    public $userName='';
    public $password ='';
    public $email ='';
    public $gender ='';

    public function setData($data=''){
        if(array_key_exists('id',$data)){
            $this->id =$data['id'];
        }
        if(array_key_exists('userName',$data)){
            $this->userName =$data['userName'];
        }
        if(array_key_exists('password',$data)){
            $this->password =$data['password'];
        }
        if(array_key_exists('email',$data)){
            $this->email =$data['email'];
        }
        if(array_key_exists('gender',$data)){
            $this->gender =$data['gender'];
        }
        return $this;
    }
    public function Store(){
        try{

            $pdo = new PDO('mysql:host=localhost;dbname=php','root','');

            $query ="INSERT INTO `students` (`id`, `username`, `password`, `email`, `gender`) VALUES (:a,:b,:c,:d,:e)";
            $stmt=$pdo->prepare($query);

            $data = $stmt->execute(array(

                ':a'=>null,
                ':b'=>$this->userName,
                ':c'=>$this->password,
                ':d'=>$this->email,
                ':e'=>$this->gender
            ));
            session_start();
            if($data)
            {
               $_SESSION['InsertMessage']='Data inserted successfully';
                header("location:create.php");
            }
        }
        catch(PDOException $e){

            echo"Error".$e->getMessage();

        }
    }
    public function index(){
        try{

            $pdo = new PDO('mysql:host=localhost;dbname=php','root','');

            $query ="SELECT *FROM students";
            $stmt=$pdo->prepare($query);

            $stmt->execute();

            $result =$stmt->fetchAll();
            if($result)
            {
                return $result;
                header("location:index.php");
            }
        }
        catch(PDOException $e){

            echo"Error".$e->getMessage();

        }
    }
    public function show(){
        try{

            $pdo = new PDO('mysql:host=localhost;dbname=php','root','');

            $query ="SELECT *FROM students WHERE id=$this->id";
            $stmt=$pdo->prepare($query);

            $stmt->execute();

            $result =$stmt->fetch();
            if($result)
            {
                return $result;
                header("location:view.php");
            }
        }
        catch(PDOException $e){

            echo"Error".$e->getMessage();

        }
    }
    public function delete(){
        try{

            $pdo = new PDO('mysql:host=localhost;dbname=php','root','');

            $query ="DELETE FROM students WHERE id=$this->id";
            $stmt=$pdo->prepare($query);

            $stmt->execute();

            $result =$stmt->fetch();

            session_start();
            $_SESSION['deletemsg']='Successfully Deleted';
            include'index.php';


        }
        catch(PDOException $e){

            echo"Error".$e->getMessage();

        }
    }
    public function update(){
        try{

            $pdo = new PDO('mysql:host=localhost;dbname=php','root','');

            $query ="UPDATE `students` SET `username` = :b, `password` =:c, `email` = :d, `gender` =:e WHERE `students`.`id` =$this->id";
            $stmt=$pdo->prepare($query);

            $data = $stmt->execute(array(

                ':b'=>$this->userName,
                ':c'=>$this->password,
                ':d'=>$this->email,
                ':e'=>$this->gender
            ));
            session_start();
           $_SESSION['UpdatetMessage']='Updated successfully';
           header('location:index.php');
        }
        catch(PDOException $e){

            echo"Error".$e->getMessage();

        }
    }
}