<?php

include_once("../../../../vendor/autoload.php");
use App\Bitm\SEIP\Students\Students;

$obj= new Students();

session_start();
if(isset($_POST))
{
    if(!empty($_POST['userName']))
    {
            $obj->setData($_POST)->Store();
    }
    else{
        $_SESSION['username']='User Name is required';
        header("location:create.php");
    }
}
else
{
    $_SESSION['username']='Invalid input';
    header("location:create.php");
}