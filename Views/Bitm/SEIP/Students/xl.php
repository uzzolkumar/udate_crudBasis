<?php
error_reporting(E_ALL);
error_reporting(E_ALL & ~E_DEPRECATED);
ini_set('display_errors',TRUE);
ini_set('display_sartup_errors',TRUE);
date_default_timezone_set('Europe/London');
if(PHP_SAPI =='cli')
die('this example should only be run from a web Brower');
include"../../../../vendor/phpoffice/phpexcel/Classes/PHPExcel.php";

include_once"../../../../vendor/autoload.php";
use App\Bitm\SEIP\Students\Students;
$obj= new Students();

$alldata = $obj->index();

$exceal = new PHPExcel();

$exceal->getProperties()->setCreator("Uzzol kumar")
    ->setLastModifiedBy('uzzol kumar')
    ->setTitle("Msoffice 2007 XLXS Test Document")
    ->setSubject("Test Document for office 2007")
    ->setDescription("office 2007 openxml php")
    ->setKeywords("office 2007 opnen")
    ->setCategory("test result file");

$exceal->setActiveSheetIndex(0)
    ->setCellValue('A1','SL')
    ->setCellValue('B1','User Name')
    ->setCellValue('C1','Email')
    ->setCellValue('D1','Gender');

$counter=2;
$serial = 0;
foreach($alldata as $data)
{
    $serial++;
    $exceal->setActiveSheetIndex(0)
        ->setCellValue('A1'.$counter,$serial)
        ->setCellValue('B1'.$counter,$data['username'])
        ->setCellValue('C1'.$counter,$data['email'])
        ->setCellValue('D1'.$counter,$data['gender']);
    $counter++;
}

$exceal->getActiveSheet()->setTitle("User List");
$exceal->setActiveSheetIndex(0);

header('Content-type:application/vnd.ms-excel');
header('Content-Disposition:attachment;filename="01simple.xls"');
header('Cache-Control:max-age=1');

header('Expires :Mon,26 Jul 1997 05:00:00 GMT');
header('Last-Modified:cache,must-revalidate');
header('Pragma:public');
$objwriter= PHPExcel_IOFactory::createWriter($exceal,'Excel5');
$objwriter->save('php://output');
exit;

