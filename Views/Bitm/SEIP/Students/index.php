<?php
include_once("../../../../vendor/autoload.php");
use App\Bitm\SEIP\Students\Students;
$obj= new Students();

$result = $obj->index();
if(isset( $_SESSION['deletemsg']))
{
    echo  $_SESSION['deletemsg'].'</br>';
    unset($_SESSION['deletemsg']);
}

if(isset( $_SESSION['UpdatetMessage'])){

    echo $_SESSION['UpdatetMessage']='Updated successfully'.'</br>';
    unset($_SESSION['UpdatetMessage']);
}

?>
<html>
<head>
    <title>
        Views information
    </title>
</head>
<body>
<span id="utility" >Download As <a href="pdf.php">PDF</a> |  <a href="xl.php">XL</a> | <a href="create.php">Add Information</a></span>
        <table border="1">
            <tr>
                <td>Serial</td>
                <td>User Name</td>
                <td>Password</td>
                <td>E-mail</td>
                <td>Gender</td>
                <td>Action</td>
            </tr>
            <?php
            if($result )
            {
            $c=1;
            foreach($result as $key=>$item)
            {
            ?>
            <tr>
                <td><?php echo $c++; ?></td>
                <td><?php echo $item['username'] ;?> </td>
                <td><?php echo $item['password'] ;?> </td>
                <td><?php echo $item['email'] ; ?>   </td>
                <td><?php echo $item['gender'] ; ?>  </td>
                <td>
                    <a href="view.php?id=<?php echo $item['id'];?>">View</a>
                    |
                    <a href="edit.php?id=<?php echo $item['id'];?>">Edit </a>
                    |
                    <a href="delete.php?id=<?php echo $item['id'];?>">Delete</a>
                </td>
            </tr>
         <?php }  } ?>
        </table>
</body>

</html>